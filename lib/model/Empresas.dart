import 'package:softcoletor/model/EmpresaModel.dart';

class Empresas{

  final List<dynamic> listaEmpresa;

  Empresas({this.listaEmpresa});

  factory Empresas.fromJson(Map<String, dynamic> json){
    var list = json['EMPRESAS'] as List;
    List<EmpresaModel> listEmpresas = list.map((i) => EmpresaModel.fromJson(i)).toList();

    return Empresas(
        listaEmpresa: listEmpresas
    );
  }
}