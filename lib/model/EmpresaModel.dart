class EmpresaModel{

  final String codigo;
  final String nome;
  final String referencia_preco;

  EmpresaModel({this.codigo, this.nome, this.referencia_preco});

  factory EmpresaModel.fromJson(Map<String, dynamic> parsedJson){
    return EmpresaModel(
      codigo: parsedJson['codigo'],
      nome: parsedJson['nome'],
      referencia_preco: parsedJson['referencia_preco']
    );
  }
}