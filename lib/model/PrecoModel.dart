
class PrecoModel{

  final String preco;
  final String loja;

  PrecoModel({this.preco, this.loja});

  factory PrecoModel.fromJson(Map<String, dynamic> parsedJson){
    return PrecoModel(
      preco: parsedJson['preco'],
      loja: parsedJson['empresa']
    );
  }
}