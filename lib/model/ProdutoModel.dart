import 'dart:convert';
import 'dart:ffi';

import 'package:softcoletor/model/EstoqueModel.dart';

class Produto{

  final String codigoEan;
  final String descricao;
  final String venda;
  final String promocao;
  final List<dynamic> estoque;

  Produto({this.codigoEan, this.descricao, this.venda, this.estoque, this.promocao});

  factory Produto.fromJson(Map<String, dynamic> json){
    var list = json['ESTOQUE'] as List;
    List<EstoqueModel> listEstoque = list.map((i) => EstoqueModel.fromJson(i)).toList();

    return Produto(
        codigoEan: json['ITEM_BARRAS'],
        descricao: json['ITEM_DESCRICAO'],
        venda: json['PLANOPRE_PRECO_VENDA'],
        promocao: json['PRECO_PROMOCAO'],
        estoque: listEstoque
    );
  }
}