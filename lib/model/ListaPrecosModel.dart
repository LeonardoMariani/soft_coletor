import 'package:softcoletor/model/EmpresaModel.dart';
import 'package:softcoletor/model/PrecoModel.dart';

class ListaPrecosModel{

  final List<dynamic> listaPrecos;

  ListaPrecosModel({this.listaPrecos});

  factory ListaPrecosModel.fromJson(Map<String, dynamic> json){
    var list = json['precosLojas'] as List;
    List<PrecoModel> listPrecos = list.map((i) => PrecoModel.fromJson(i)).toList();

    return ListaPrecosModel(
        listaPrecos: listPrecos
    );
  }
}