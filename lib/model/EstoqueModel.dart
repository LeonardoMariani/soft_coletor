
class EstoqueModel{

  final String loja;
  final String quantidade;
  final String custo;

  EstoqueModel({this.loja, this.quantidade,this.custo});

  factory EstoqueModel.fromJson(Map<String, dynamic> parsedJson){
    return EstoqueModel(
      loja: parsedJson['empresa'],
      quantidade: parsedJson['estoque'],
      custo: parsedJson['custo']
    );
  }
}