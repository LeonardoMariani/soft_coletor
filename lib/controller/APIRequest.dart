import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:softcoletor/model/Empresas.dart';
import 'package:softcoletor/model/PrecoModel.dart';
import 'package:softcoletor/model/ListaPrecosModel.dart';
import 'package:softcoletor/model/ProdutoModel.dart';
import 'package:softcoletor/model/Vars.dart' as Vars;
import 'package:softcoletor/view/Precos.dart';

class APIRequest{

  static Future<bool> login(String usuario, String senha) async {

   bool _retornoLogin = false;

   if(usuario == Vars.UserPadrao && senha == Vars.PassPadrao)
     _retornoLogin = true;

    return _retornoLogin;
  }

  static Future<Produto> getProdutos(String codigoEan, String loja, int custo) async{

    var produtoUrl = "http://" + Vars.IP +":" + Vars.Porta +  "/consulta/firebird_json_promo.php";
    var header = {"Content-Type" : "application/json"};
    var response;
    var retorno = 0;
    Map params = {
      "codigo": codigoEan.padLeft(13,'0'),
      "loja": loja,
      "custo": custo
    };
    var _body = json.encode(params);
    try{
      response = await http.post(produtoUrl, headers: header, body: _body);
      retorno = 1;
    }catch(ex){
      retorno = 500;
    }
    if(retorno == 500 || retorno == 0 || response.body == '-1')
      return throw("Produto não encontrado");
    else if((response.statusCode == 201 || response.statusCode == 200) && response.body.length > 30)
      return Produto.fromJson(json.decode(response.body));
  }

  Future<Empresas> getEmpresas() async{

    var empresaUrl = "http://" + Vars.IP +":" + Vars.Porta +  "/consulta/get_empresa.php";
    var header = {"Content-Type" : "application/json"};
    var response;
    var retorno = 0;

    try{
      response = await http.get(empresaUrl, headers: header);
    }catch(ex){
      retorno = 500;
    }
    if(retorno == 500)
      return throw("Erro na conexão");
    else if((response.statusCode == 201 || response.statusCode == 200) && response.body.length > 30)
      return Empresas.fromJson(json.decode(response.body));

  }

  static Future<ListaPrecosModel> getPrecos(String ean) async {

    var precoUrl = "http://" + Vars.IP +":" + Vars.Porta +  "/consulta/get_precos.php";
    var header = {"Content-Type" : "application/json"};
    var response;
    var retorno = 0;
    Map params = {
      "codigo": ean.padLeft(13,'0'),
    };
    var _body = json.encode(params);
    try{
      response = await http.post(precoUrl, headers: header, body: _body);
      retorno = 1;
    }catch(ex){
      retorno = 500;
    }
    if(retorno == 500 || retorno == 0 || response.body == '-1')
      return throw("Preço não encontrado");
    else if((response.statusCode == 201 || response.statusCode == 200) && response.body.length > 30)
      return ListaPrecosModel.fromJson(json.decode(response.body));
  }
}