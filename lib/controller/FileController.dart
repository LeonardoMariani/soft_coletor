import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:softcoletor/model/Vars.dart' as Vars;

class FileController{

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }
  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/teste8.txt');
  }
  Future<String> readCounter() async {
    try {
      final file = await _localFile;

      // Read the file.
      String contents = await file.readAsString();

      return contents;
    } catch (e) {
      // If encountering an error, return 0.
      return null;
    }
  }
  Future<File> writeCounter(String ip, String porta, bool custo, int precoCustoSelecionado, int lojaPadrao) async {
    final file = await _localFile;

    String dataFinal = '$ip' + ',' + '$porta' + ',' + '$custo' + ',' + '$precoCustoSelecionado' + ',' + '$lojaPadrao';

    return file.writeAsString(dataFinal);
  }
}