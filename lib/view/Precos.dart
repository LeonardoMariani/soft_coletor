import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:softcoletor/controller/APIRequest.dart';
import 'package:softcoletor/model/EmpresaModel.dart';
import 'package:softcoletor/model/ListaPrecosModel.dart';
import 'package:softcoletor/model/PrecoModel.dart';

class Precos extends StatefulWidget {

  final String descricao;
  final String ean;
  final List<EmpresaModel> listaEmpresas;
  const Precos({Key key, @required this.descricao, @required this.ean, @required this.listaEmpresas}): super(key: key);
  @override
  _PrecosState createState() => _PrecosState();
}

class _PrecosState extends State<Precos> {

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _descricao;
  String _ean;
  List<PrecoModel> listaPrecos;
  RegExp exp;
  List<EmpresaModel> _listaEmpresa;

  void initState(){
    super.initState();
    _descricao = widget.descricao;
    _ean = widget.ean;
    _listaEmpresa = widget.listaEmpresas;
  }

  void mostrarVersao(String codLoja) {
    String nomeLoja = "";

    _listaEmpresa.forEach((loja) => loja.codigo == codLoja ? nomeLoja = loja.nome : null);

    if(nomeLoja.length > 2){
      _scaffoldKey.currentState.showSnackBar(
        SnackBar(
          duration: Duration(seconds: 3),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),
          behavior: SnackBarBehavior.floating,
          backgroundColor: Colors.black,
          content: Text('$nomeLoja'),
          action: SnackBarAction(
            textColor: Color(0xFFEE1C25),
            label: 'Fechar',
            onPressed: () {},
          ),
        ),
      );
    }
  }
  @override
  Widget build(BuildContext context) {

    final titulo = FittedBox(
      fit: BoxFit.fitWidth,
      child: Text.rich(
          TextSpan(
              text: "Preços",
              style: GoogleFonts.libreBaskerville(
                  textStyle: TextStyle(fontSize: 50,color: Color(0xFFEE1C25),
                  )
              )
          )
      ),
    );

    final textDesc = Text.rich(
      TextSpan(
        text: "$_descricao",
        style: TextStyle(fontSize: 25, color: Colors.black, backgroundColor: Color(0xFFAF815B)),
      ),
      textAlign: TextAlign.center,
    );


    final voltarButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xFFEE1C25),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text("Voltar",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Form(
      child: Scaffold(
        key: _scaffoldKey,
        bottomSheet: new Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: voltarButton,
            )
        ),
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
          child: Center(
            child: Container(
              child: Column(
                children: <Widget>[
                  SizedBox(height: MediaQuery.of(context).size.height/20),
                  titulo,
                  SizedBox(height: MediaQuery.of(context).size.height/50),
                  textDesc,
                  SizedBox(height: MediaQuery.of(context).size.height/50),
                  FutureBuilder<ListaPrecosModel>(
                    future: APIRequest.getPrecos(_ean).catchError((onError){
                      return Text("Erro de conexão - 1",
                          style: TextStyle(color: Colors.red, fontSize: 20)
                      );
                    }).timeout(Duration(seconds: 10), onTimeout: (){
                      return null;
                    }),
                    builder: (context, snapshot){
                      if(snapshot.connectionState == ConnectionState.waiting){
                        return Text("Carregando dados...",
                            style: TextStyle(color: Colors.red, fontSize: 20)
                        );
                      }else if(snapshot.hasError){
                        return Text("Erro de conexão - 2",
                            style: TextStyle(color: Colors.red, fontSize: 20)
                        );
                      }else if(snapshot.data == null){
                        return Text("Erro de conexão - 3",
                            style: TextStyle(color: Colors.red, fontSize: 20)
                        );
                      }else{
                        //listaPrecos = snapshot.data.preco;
                        return (DataTable(
                          columnSpacing: 10,
                          dataRowHeight: 40,
                          columns: [
                            DataColumn(label: Text.rich(
                                TextSpan(
                                    text: "Loja",
                                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)
                                )
                            )
                            ),
                            DataColumn(label: Text.rich(
                                TextSpan(
                                    text: "Preço",
                                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)
                                )
                            )
                            )
                          ],
                          rows: snapshot.data.listaPrecos.map((elemento)=> DataRow(
                            cells: <DataCell>[
                              DataCell(
                                  Text.rich(//Loja
                                  TextSpan(
                                      recognizer: new TapGestureRecognizer()..onTap = () {mostrarVersao(elemento.loja);},
                                      text: (elemento.loja).toString(),
                                      style: TextStyle(fontSize: 22)
                                  )
                              )
                              ),
                              DataCell(Align(//Precos
                                alignment: Alignment.center ,
                                child: Text.rich(
                                    TextSpan(
                                        text: 'R\$'+ double.parse(elemento.preco).toStringAsFixed(2),
                                        style: TextStyle(color: Colors.green, fontSize: 22)
                                    )
                                ),
                              )
                              )
                            ],
                          )).toList(),
                        ));
                      }
                    },
                  ),
                  //SizedBox(height: MediaQuery.of(context).size.height/30),
                  //voltarButton
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
