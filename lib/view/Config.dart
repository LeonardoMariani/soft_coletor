import 'package:flutter/gestures.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:softcoletor/controller/FileController.dart';
import 'package:softcoletor/model/Vars.dart' as Vars;

class Config extends StatefulWidget {
  @override
  _ConfigState createState() => _ConfigState();
}

class _ConfigState extends State<Config> {

  final _ctrlIP = TextEditingController(text: Vars.IPSet ? Vars.IP : "");
  final _ctrlPorta = TextEditingController(text: Vars.IPSet ? Vars.Porta : "");
  bool _ctrlExibirCusto = Vars.exibirCusto;
  final _ctrlForm = GlobalKey<FormState>();
  bool _retornoIP = true;
  FileController fileController = new FileController();
  List _precos = ["Custo unitario", "Custo direto", "Custo real", "Custo medio", "Custo simples", "Custo usuário"];
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<DropdownMenuItem<String>> _dropDownMenuItems;
  String _precoCustoSelecionado;
  int _indexPrecoCustoSelecionado = 0;

  void mostrarVersao() {
    _scaffoldKey.currentState.showSnackBar(
      SnackBar(
        backgroundColor: Colors.black,
        content: Text('Versão do aplicativo: v2.0'),
        action: SnackBarAction(
          textColor: Color(0xFFEE1C25),
          label: 'Fechar',
          onPressed: () {},
        ),
      ),
    );
  }
  void initState(){
    _dropDownMenuItems = getDropDown();
    _precoCustoSelecionado = _dropDownMenuItems[Vars.tipoCusto].value;
    super.initState();
  }

  List<DropdownMenuItem<String>> getDropDown(){
    List<DropdownMenuItem<String>> item = new List();
    for(String custo in _precos){
      item.add(new DropdownMenuItem(
          child: new Text(custo),
          value: custo
        )
      );
    }
    return item;
  }

  @override
  Widget build(BuildContext context) {

    final titulo = FittedBox(
      fit: BoxFit.fitWidth,
      child: Text.rich(
          TextSpan(
              recognizer: new LongPressGestureRecognizer()..onLongPress = () {mostrarVersao();},
              text: "Configuração",
              style: GoogleFonts.libreBaskerville(
                  textStyle: TextStyle(fontSize: 50,color: Color(0xFFEE1C25),
                  )
              )
          )
      ),
    );

    final ip = TextFormField(
      validator: (value){
        if(value.isEmpty)
          return "Digite um IP para configuração";
        else
          return null;
      },
      obscureText: false,
      controller: _ctrlIP,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Digite o IP",
        prefixIcon: const Icon(Icons.wifi,color: Colors.red),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(52),
            borderSide: BorderSide(color: Colors.red)
        ),
        border:
        OutlineInputBorder(borderRadius: BorderRadius.circular(52),
          borderSide: BorderSide(color: Colors.white),
        ),
      ),
    );

    final porta = TextFormField(
      validator: (value){
        if(value.isEmpty)
          return "Digite uma porta para configuração";
        else
          return null;
      },
      obscureText: false,
      controller: _ctrlPorta,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Digite a porta",
        prefixIcon: const Icon(Icons.wifi,color: Colors.red),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(52),
            borderSide: BorderSide(color: Colors.red)
        ),
        border:
        OutlineInputBorder(borderRadius: BorderRadius.circular(52),
          borderSide: BorderSide(color: Colors.white),
        ),
      ),
    );

    final salvarButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xFFEE1C25),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          _salvarIp(context);
        },
        child: Text("Salvar",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold
            )
        ),
      ),
    );


    final voltarButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.black54,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text("Voltar",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    final switchCusto = new SwitchListTile(
      title: Text("Exibir custo do produto?"),
        value: _ctrlExibirCusto,
        activeColor: Colors.red,
        onChanged: (value){
          setState(() {
            _ctrlExibirCusto = value;
          });
        }
    );

    final dropDownCusto = new Row(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Text("Selecione o custo:",
            style: TextStyle(
                fontSize: 16
            ),
          ),
        ),
        DropdownButton(
          value: _precoCustoSelecionado,
          items: _dropDownMenuItems,
          onChanged: (value){
            setState(() {
              _precoCustoSelecionado = value;
              _indexPrecoCustoSelecionado = _precos.indexOf(value);
            });
          },
          icon: Icon(Icons.keyboard_arrow_down),
        )
      ],
    );

    final textRetornoAdd = Visibility(
        visible: !_retornoIP,
        child: Text(
          _retornoIP ? "IP salvo com sucesso" : "Erro ao salvar IP",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: _retornoIP ? Colors.green : Colors.red
          ),
        ),
      );

    return Form(
        key: _ctrlForm,
        child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: Center(
              child: Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: MediaQuery.of(context).size.height/30),
                    titulo,
                    SizedBox(height: MediaQuery.of(context).size.height/40),
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                      child: ip,
                    ),
                    porta,
                    switchCusto,
                    dropDownCusto,
                    SizedBox(height: MediaQuery.of(context).size.height/40),
                    salvarButton,
                    SizedBox(height: MediaQuery.of(context).size.height/40),
                    voltarButton,
                  ],
                ),
              ),
            ),
          ),
        )
    );
  }

  void _salvarIp(BuildContext context) {
    bool formOk = _ctrlForm.currentState.validate();
    
    if(formOk){
      try{
        fileController.writeCounter(_ctrlIP.text,_ctrlPorta.text, _ctrlExibirCusto, _indexPrecoCustoSelecionado, 0);
        Vars.IP = _ctrlIP.text;
        Vars.IPSet = true;
        Vars.Porta = _ctrlPorta.text;
        Vars.exibirCusto = _ctrlExibirCusto;
        Vars.tipoCusto = _indexPrecoCustoSelecionado;
        Vars.indexLojaSelecionada = 0;
        FocusScope.of(context).unfocus();
        showAlertDialog(context);
      }catch(e){
        setState(() {
          _retornoIP = false;
        });
      }
    }
  }
  showAlertDialog(BuildContext context) {
    // Create button
    Widget okButton = FlatButton(
      child: Text("Ok", style: TextStyle(color: Colors.red)),
      onPressed: () {
        Navigator.of(context).pop();
        //Navigator.pop(context);
      },
    );

    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text("Configurações salvas com sucesso."),
      actions: [
        okButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
