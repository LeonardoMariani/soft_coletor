import 'package:flutter/material.dart';
import "package:barcode_scan/barcode_scan.dart";
import 'package:softcoletor/controller/APIRequest.dart';
import 'package:softcoletor/controller/FileController.dart';
import 'package:softcoletor/model/EmpresaModel.dart';
import 'package:softcoletor/model/Empresas.dart';
import 'package:softcoletor/model/ProdutoModel.dart';
import 'package:softcoletor/view/Estoque.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:softcoletor/model/Vars.dart' as Vars;
import 'package:softcoletor/view/Precos.dart';

class Inicial extends StatefulWidget {
  @override
  _InicialState createState() => _InicialState();
}

class _InicialState extends State<Inicial> {
  APIRequest apiRequest = new APIRequest();
  String qrSpan = " ";
  Produto _produtoModel;
  String _ean = "";
  String _descricao = "";
  String _venda = "";
  String _promocao = "";
  String _custo = "";
  bool _erro = true;
  bool _temPromocao = false;
  TextEditingController _ctrlEAN = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  List _lojas;
  List<DropdownMenuItem<EmpresaModel>> _dropDownMenuItems;
  EmpresaModel _lojaSelecionada;
  int _indexlojaSelecionada = 0;
  List<EmpresaModel> _listaEmpresa;
  FileController fileController = new FileController();
  bool _lerCodigo = false;
  bool _digitarCodigo = false;
  Empresas futureEmpresas;
  String ip = "";

  digitarEANDialog(BuildContext context){
    setState(() {
      _ctrlEAN.text = "";
    });
    Widget cancelarButton = FlatButton(
      child: Text("Cancelar", style: TextStyle(color: Colors.red),),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    Widget consultarButton = FlatButton(
      child: Text("Consultar", style: TextStyle(color: Colors.red),),
      onPressed: () {
        bool formOk = _formKey.currentState.validate();
        if(formOk){
          _getProduto(_ctrlEAN.text, _lojaSelecionada.referencia_preco);
          Navigator.of(context).pop();
        }
      },
    );
    Widget textForm = TextFormField(
      autofocus: true,
      validator: (value){
        if(value.length != 13 && value.length != 8)
          return "Código inválido";
        else
          return null;
      },
      obscureText: false,
      controller: _ctrlEAN,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Digite o código EAN",
        prefixIcon: const Icon(Icons.search,color: Colors.red),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(52),
            borderSide: BorderSide(color: Colors.red)
        ),
        border:
        OutlineInputBorder(borderRadius: BorderRadius.circular(52),
          borderSide: BorderSide(color: Colors.white),
        ),
      ),
    );


    return showDialog(context: context, builder: (context){
      return AlertDialog(
        content: new Form(
            key: _formKey,
            child: textForm
        ),
        actions: <Widget>[
          cancelarButton,
          consultarButton,
        ],
      );
    });
  }

  void initState(){
    //_dropDownMenuItems = getDropDown();
    //_lojaSelecionada = _dropDownMenuItems[Vars.indexLojaSelecionada].value;
    super.initState();
    ip = Vars.IP;
  }

  List<DropdownMenuItem<int>> getDropDown(List listaEmpresa){
    _listaEmpresa = listaEmpresa;
    List<DropdownMenuItem<int>> item = new List();
    for(EmpresaModel empresa in _listaEmpresa){
      item.add(new DropdownMenuItem(
          value: int.parse(empresa.codigo),
          child: Row(
            children: <Widget>[
              //Icon(Icons.home),
              FittedBox(
                  fit: BoxFit.fitWidth,
                  child:Text(
                      empresa.nome,
                      style: TextStyle(fontSize: 15, color: Color(0xFFAF815B), fontWeight: FontWeight.bold)
                  )
              )
              //Text(empresa.nome, style: TextStyle(fontSize: 10),)
            ],
          ),
      )
      );
    }
    return item;
  }
  @override
  Widget build(BuildContext context) {

    final titulo = FittedBox(
      fit: BoxFit.fitWidth,
      child: Text.rich(
          TextSpan(
              text: "Soft Consulta",
              style: GoogleFonts.libreBaskerville(
                  textStyle: TextStyle(fontSize: 50,color: Color(0xFFEE1C25),
                  )
              )
          )
      ),
    );

    final textCodigo = Text.rich(
      TextSpan(
        text: "Código \n",
        style: TextStyle(fontSize: 22, color: Colors.black),
        children: [
          TextSpan(text: _ean, style: TextStyle(color: _erro ? Colors.red : Colors.black, fontSize: 30))
        ]
      ),
      textAlign: TextAlign.center,
    );

    final textLoja =  new Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          child: Text("Selecione a loja padrão",
            style: TextStyle(fontSize: 22, color: Colors.black)
          ),
        ),
        DropdownButton(
          value: _lojaSelecionada,
          items: _dropDownMenuItems,
          onChanged: (value){
            setState(() {
              _lojaSelecionada = value;
              _indexlojaSelecionada = _lojas.indexOf(value);
              Vars.indexLojaSelecionada = _indexlojaSelecionada;
            });
          },
          icon: Icon(Icons.arrow_drop_down_circle),

        )
      ],
    );

    final digitarConsultaButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xFFEE1C25),
      child: MaterialButton(
        minWidth: 5,
        onPressed: () {
          if(_digitarCodigo)
            digitarEANDialog(context);
        },
        child:Icon(Icons.mode_edit, color: Colors.white)
      ),
    );

    final textDesc = Text.rich(
      TextSpan(
        text: "Descrição \n ",
        style: TextStyle(fontSize: 22, color: Colors.black),
        children: <TextSpan>[
          TextSpan(text: _descricao, style: TextStyle(
              backgroundColor: Color(0xFFAF815B),
              color: Colors.black
            )
          )
        ]
      ),
      textAlign: TextAlign.center,
    );

    final textVenda = Text.rich(
      TextSpan(
        text: "Preço de venda \n",
        style: TextStyle(fontSize: 22, color: Colors.black),
        children: <TextSpan>[
          TextSpan(
              text: _venda,
              style: _temPromocao ? TextStyle(color: Colors.green, fontSize: 25, fontWeight: FontWeight.bold) :
                                    TextStyle(color: Colors.green, fontSize: 35, fontWeight: FontWeight.bold)
          )
        ]
      ),
      textAlign: TextAlign.center,
    );

    final textPromo = Text.rich(
      TextSpan(
          text: "Preço da promoção \n",
          style: _temPromocao ? TextStyle(fontSize: 22, color: Colors.black) : TextStyle(fontSize: 22, color: Colors.white),
          children: <TextSpan>[
            TextSpan(
                text: _promocao,
                style: _temPromocao ? TextStyle(color: Colors.red, fontSize: 35, fontWeight: FontWeight.bold) :
                TextStyle(color: Colors.white, fontSize: 35, fontWeight: FontWeight.bold)
            )
          ]
      ),
      textAlign: TextAlign.center,
    );

    final estoqueButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color:  Color(0xFFEE1C25),
      child: MaterialButton(
        minWidth: 5,
        onPressed: () {
          //print(_produtoModel.estoque);
          if(!_erro)
            Navigator.push(context, MaterialPageRoute(builder: (context)=> Estoque(produto: _produtoModel)));
        },
        child: Icon(Icons.store, color: Colors.white,)
      ),
    );

    final precosButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color:  Color(0xFFEE1C25),
      child: MaterialButton(
          minWidth: 5,
          onPressed: () {
            if(!_erro)
              Navigator.push(context, MaterialPageRoute(builder: (context)=> Precos(descricao: _produtoModel.descricao, ean: _produtoModel.codigoEan, listaEmpresas: _listaEmpresa)));
          },
          child: Icon(Icons.attach_money, color: Colors.white,)
      ),
    );

    final consultarCameraButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xFFEE1C25),
      child: MaterialButton(
        minWidth: 5,
        onPressed: () {
          if(_lerCodigo)
            _scanQR();
        },
        child: Icon(Icons.photo_camera, color: Colors.white)
      ),
    );

    final iconList = DefaultTextStyle.merge(
      child: Container(
        padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                consultarCameraButton,
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 0) ,
                  child: Text("Ler código"),
                )
              ],
            ),
            Column(
              children: [
                digitarConsultaButton,
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 0) ,
                  child: Text("Digitar código"),
                )
              ],
            ),
            Column(
              children: [
                estoqueButton,
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 0) ,
                  child: Text("Ver estoque"),
                )
              ],
            ),
            Column(
              children: [
                precosButton,
                Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 0) ,
                  child: Text("Ver preços"),
                )
              ],
            ),
          ],
        ),
      ),
    );

    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        bottomSheet: BottomAppBar(
          color: Color(0xFFAF815B),
          child: Container(
            height: 100,
            child: iconList,
          ),
        ),
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: MediaQuery.of(context).size.height/20),
                      titulo,
                      Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: MediaQuery.of(context).size.height/25),
                              Column(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: Text("Selecione a loja padrão",
                                        style: TextStyle(fontSize: 22, color: Colors.black)
                                    ),
                                  ),
                                  FutureBuilder<Empresas>(
                                    future: this.getEmpresas().catchError((onError){
                                      return Text("Erro de conexão - 1",
                                          style: TextStyle(color: Colors.red, fontSize: 20)
                                      );
                                    }).timeout(Duration(seconds: 10), onTimeout: (){
                                      return null;
                                    }),
                                    builder: (context, snapshot){
                                      if(snapshot.connectionState == ConnectionState.waiting && this.futureEmpresas == null){
                                        return Text("Carregando lojas...",
                                            style: TextStyle(color: Colors.red, fontSize: 20)
                                        );
                                      }else if(snapshot.hasError){
                                        return Text("Erro de conexão - 2",
                                            style: TextStyle(color: Colors.red, fontSize: 20)
                                        );
                                      }else if(snapshot.data == null){
                                        return Text("Erro de conexão - 3",
                                            style: TextStyle(color: Colors.red, fontSize: 20)
                                        );
                                      }else{
                                        _digitarCodigo = true;
                                        _lerCodigo = true;
                                        _listaEmpresa = snapshot.data.listaEmpresa;
                                        _lojaSelecionada = _listaEmpresa[Vars.indexLojaSelecionada];
                                        return DropdownButton<EmpresaModel>(
                                          onChanged: (EmpresaModel empresa){
                                            setState(() {
                                              _lojaSelecionada = empresa;
                                              _indexlojaSelecionada = _listaEmpresa.indexOf(empresa);
                                              Vars.indexLojaSelecionada = _indexlojaSelecionada;
                                              _ean = "";
                                              _descricao = "";
                                              _venda = "";
                                              _erro = true;
                                            });
                                            fileController.writeCounter(ip,Vars.Porta, Vars.exibirCusto, Vars.tipoCusto, Vars.indexLojaSelecionada);
                                          },
                                          value: _lojaSelecionada,
                                          items: _listaEmpresa.map((EmpresaModel empresa){
                                            return DropdownMenuItem<EmpresaModel>(
                                                value: empresa,
                                                child: new FittedBox(
                                                    fit: BoxFit.fitWidth,
                                                    child:Text(
                                                        empresa.nome,
                                                        style: TextStyle(fontSize: 15, color: Color(0xFFAF815B),
                                                            fontWeight: FontWeight.bold
                                                        )
                                                    )
                                                )
                                            );
                                          }
                                          ).toList(),
                                        );
                                      }
                                    },
                                  )
                                ],
                              ),
                              SizedBox(height: MediaQuery.of(context).size.height/15),
                              textCodigo,
                              SizedBox(height: MediaQuery.of(context).size.height/15),
                              textDesc,
                              SizedBox(height: MediaQuery.of(context).size.height/15),
                              textVenda,
                              SizedBox(height: MediaQuery.of(context).size.height/35),
                              textPromo,
                            ],
                          )
                      ),
                    ],
                  ),
                )
            )
          )
      ),
    );
  }

  Future<Empresas> getEmpresas() async{

    if(this.futureEmpresas == null)
      this.futureEmpresas = await apiRequest.getEmpresas();

    return this.futureEmpresas;
  }

  Future _scanQR() async {
    try {
      String qrResult = await BarcodeScanner.scan();
      //String qrResult = "7891098000026";
      if(qrResult.length == 13 || qrResult.length == 8){
        _getProduto(qrResult, _lojaSelecionada.referencia_preco);
      }else{
        erroLeitura("Código de barras invalido");
      }
    } catch(ex){
      if(ex.message != 'Invalid envelope')
        erroLeitura("Erro na leitura");
    }
  }

  void erroLeitura(String retorno){
    setState(() {
      _ean = retorno;
      _descricao = "";
      _custo = "";
      _venda = "";
      _promocao = "";
      _temPromocao = false;
      _erro = true;
    });
  }
  void _getProduto(String qrResult, String loja){

    setState(() {
      _ean = "Procurando produto...";
      _descricao = "";
      _custo = "";
      _venda = "";
      _promocao = "";
      _temPromocao = false;
      _erro = true;
    });
    APIRequest.getProdutos(qrResult, loja, Vars.tipoCusto)
        .then((produto) => setState((){
      _produtoModel = produto;
      _ean = (int.parse(produto.codigoEan)).toString();
      _descricao = produto.descricao;
      _venda = "R\$" + double.parse(produto.venda).toStringAsFixed(2);
      if(double.parse(produto.promocao) == 0.0)
        _temPromocao = false;
      else{
        _promocao = "R\$" + double.parse(produto.promocao).toStringAsFixed(2);
        _temPromocao = true;
      }
      _erro = false;
    })).catchError((onError){
      erroLeitura(onError);
    }).timeout(Duration(seconds: 10), onTimeout:() {
      erroLeitura("Falha na conexão com o servidor");
    });
  }
}


