import 'package:flutter/material.dart';
import 'package:softcoletor/controller/APIRequest.dart';
import 'package:softcoletor/controller/FileController.dart';
import 'package:softcoletor/view/Config.dart';
import 'package:softcoletor/model/Vars.dart' as Vars;
import 'package:softcoletor/view/Inicial.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  final _ctrlLogin = TextEditingController();
  final _ctrlSenha = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  final FileController fileController = new FileController();

  @override
  Widget build(BuildContext context) {

    void initState(){
      fileController.readCounter().then((value){
        print(value);
        if(value == null){
          setState(() {
            Vars.IPSet = false;
          });
        }
        else{
          List itensCSV = value.split(',');
          setState(() {
            Vars.IP = itensCSV[0];
            Vars.Porta = itensCSV[1];
            if(itensCSV[2] == "false")
              Vars.exibirCusto = false;
            else
              Vars.exibirCusto = true;
            Vars.tipoCusto = int.parse(itensCSV[3]);
            Vars.IPSet = true;
            Vars.indexLojaSelecionada = int.parse(itensCSV[4]);
          });
        }
      });
    }

    initState();
    Future<void> _entrarButton(BuildContext context) async {
      bool formOk = _formKey.currentState.validate();

      if(formOk){
        if(Vars.IPSet){
          var response = await APIRequest.login(_ctrlLogin.text, _ctrlSenha.text);
          FocusScope.of(context).unfocus();
          if(response){
            setState(() {
              _ctrlLogin.text = "";
              _ctrlSenha.text = "";
            });
            Navigator.push(context, MaterialPageRoute(builder: (context) => Inicial()));
          }else
            showAlertDialog(context, "Tentar novamente", "Usuário não encontrado.");
        }else{
          showAlertDialog(context, "Ok", "Por favor, configure a conexão (caso tenha alguma duvida notifique o suporte)");
        }
      }
    }

    final iconSoft = new Image.asset(
        'images/logo_icon.PNG',
        fit: BoxFit.cover,
    );
    final email = TextFormField(
      validator: (value){
        if(value.isEmpty)
          return "Digite o usuário";
        else
          return null;
      },
      obscureText: false,
      controller: _ctrlLogin,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Usuário",
        prefixIcon: const Icon(Icons.person,color: Colors.red),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(52),
            borderSide: BorderSide(color: Colors.red)
        ),
        border:
        OutlineInputBorder(borderRadius: BorderRadius.circular(52),
          borderSide: BorderSide(color: Colors.white),
        ),
      ),
    );

    final senha = TextFormField(
      validator: (value){
        if(value.isEmpty)
          return "Digite a senha";
        else
          return null;
      },
      controller: _ctrlSenha,
      obscureText: true,
      decoration: InputDecoration(
        prefixIcon: const Icon(Icons.https, color: Colors.red),
        contentPadding: EdgeInsets.fromLTRB(20, 15, 20, 15),
        hintText: "Senha",
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(32),
            borderSide: BorderSide(color: Colors.red)
        ),
        border:
        OutlineInputBorder(borderRadius: BorderRadius.circular(32)),
      ),
    );


    final loginButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xFFEE1C25),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          //Navigator.push(context, EnterExitRoute(exitPage: Login(), enterPage: MainUser()));
          _entrarButton(context);
        },
        child: Text("Entrar",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    final textInfos = Column(
      children: <Widget>[
        Text(
          "Av. Visconde de Guarapuava, 1350"
        ),
        Text(
          "(41) 3263-4580"
        ),
      ],
    );

    return Form(
      key: _formKey,
        child: Scaffold(
          backgroundColor: Colors.white,
          body: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
            child: Center(
              child: Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: MediaQuery.of(context).size.height/20),
                    iconSoft,
                    SizedBox(height: MediaQuery.of(context).size.height/15),
                    email,
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: senha,
                    )                    ,
                    SizedBox(height: MediaQuery.of(context).size.height/15),
                    loginButton,
                    Padding(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: textInfos,
                    )
                  ],
                ),
              ),
            ),
          ),
          floatingActionButton: 
          FloatingActionButton(
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) => Config()));
              },
              child: Icon(Icons.settings),
            backgroundColor: Vars.IPSet ? Colors.grey : Colors.red,
          ),
        )
    );
  }
  showAlertDialog(BuildContext context, String botao, String alerta) {
    // Create button
    Widget okButton = FlatButton(
      child: Text(botao, style: TextStyle(color: Colors.red),),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      content: Text(alerta),
      actions: [
        okButton,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}