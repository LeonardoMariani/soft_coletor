import 'package:flutter/material.dart';
import 'package:softcoletor/model/EstoqueModel.dart';
import 'package:softcoletor/model/ProdutoModel.dart';
import 'package:softcoletor/model/Vars.dart' as Vars;
import 'package:google_fonts/google_fonts.dart';

class Estoque extends StatefulWidget {

  final Produto produto;
  const Estoque({Key key, @required this.produto}) : super(key: key);

  @override
  _EstoqueState createState() => _EstoqueState();

}

class _EstoqueState extends State<Estoque> {


  @override
  Widget build(BuildContext context) {

    String _textDesc = widget.produto.descricao;
    double _precoVenda = double.parse(widget.produto.venda);
    List<EstoqueModel> listasEstoques = widget.produto.estoque;

    final titulo = FittedBox(
      fit: BoxFit.fitWidth,
      child: Text.rich(
          TextSpan(
              text: "Estoque",
              style: GoogleFonts.libreBaskerville(
                  textStyle: TextStyle(fontSize: 50,color: Color(0xFFEE1C25),
                  )
              )
          )
      ),
    );

    final textDesc = Text.rich(
      TextSpan(
        text: "$_textDesc",
        style: TextStyle(fontSize: 25, color: Colors.black, backgroundColor: Color(0xFFAF815B)),
      ),
      textAlign: TextAlign.center,
    );

    final dataEstoque = Vars.exibirCusto ? (DataTable(

      columnSpacing: 10,
      dataRowHeight: 40,
      columns: [
        DataColumn(label: Text.rich(
            TextSpan(
                text: "Loja",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
            )
        )
        ),
        DataColumn(label: Text.rich(
            TextSpan(
                text: "Qtd",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
            )
        )
        ),
        DataColumn(label: Text.rich(
            TextSpan(
                text: "Custo",
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)
            )
        )
        )
      ],
      rows: listasEstoques.map((elemento)=> DataRow(
        cells: <DataCell>[
          DataCell(Text.rich(//Loja
              TextSpan(
                  text: (elemento.loja.toString()),
                  style: TextStyle(fontSize: 18)
              )
          )
          ),
          DataCell(Align(//Qtd
            alignment: Alignment.center ,
            child: Text.rich(
                TextSpan(
                    text: double.parse(elemento.quantidade).toStringAsFixed(2),
                    style: double.parse(elemento.quantidade) > 0 ?
                    TextStyle(color: Colors.green, fontSize: 18) :
                    TextStyle(color: Colors.red, fontSize: 18)
                )
            ),
          )
          ),
          DataCell(Text.rich(//Custo
              TextSpan(
                  text: "R\$ " + double.parse(elemento.custo).toStringAsFixed(2),
                  style: TextStyle(fontSize: 18, color: Colors.green)
              )
          )
          )
        ],
      )).toList(),
    )) : (DataTable(
      columnSpacing: 50,
      dataRowHeight: 60,
      columns: [
        DataColumn(label: Text.rich(
            TextSpan(
                text: "Loja",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold))
        )
        ),
        DataColumn(label: Text.rich(
            TextSpan(
                text: "Qtd",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)
            )
        )
        ),
      ],
      rows: listasEstoques.map((elemento)=> DataRow(
        cells: <DataCell>[
          DataCell(Text.rich(
              TextSpan(
                  text: elemento.loja,
                  style: TextStyle(fontSize: 18)
              )
          )
          ),
          DataCell(Align(
            alignment: Alignment.center ,
            child: Text.rich(
                TextSpan(
                    text: double.parse(elemento.quantidade).toStringAsFixed(2),
                    style: double.parse(elemento.quantidade) > 0 ?
                    TextStyle(color: Colors.green, fontSize: 18) :
                    TextStyle(color: Colors.red, fontSize: 18)
                )
            ),
          )
          )
        ],
      )).toList(),
    ));

    final voltarButton = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xFFEE1C25),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text("Voltar",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return MaterialApp(
        home: Scaffold(
              bottomSheet: new Container(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
                  child: voltarButton,
                )
              ),
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.white,
            body: SingleChildScrollView(
              padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
              child: Center(
                child: Container(
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: MediaQuery.of(context).size.height/20),
                      titulo,
                      SizedBox(height: MediaQuery.of(context).size.height/50),
                      textDesc,
                      SizedBox(height: MediaQuery.of(context).size.height/50),
                      dataEstoque,
                      //SizedBox(height: MediaQuery.of(context).size.height/30),
                      //voltarButton
                    ],
                  ),
                ),
              ),
          ),
        ),
      );
  }
}
